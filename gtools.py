#!/usr/bin/env python3

import click

from from_yaml import FromYaml
from loaders import YamlLoader
from save_as_tree import SaveAsTree


@click.group()
def gtools():
    pass


@gtools.command('from_yaml', short_help="Accounts from YAML Structure")
@click.option('-o', '--output_file')
@click.option('--overwrite/--no-overwrite', default=False)
@click.argument('input_path')
def from_yaml(input_path, output_file, overwrite):
    """Generate new file from YAML Account Structure"""
    loader = YamlLoader(input_path)
    converter = FromYaml(loader, output_path=output_file, do_overwrite=overwrite)
    converter.run()


@gtools.command('save_tree', short_help="Visualize accounts as ascii tree")
@click.option('-o', '--output_file')
@click.option('--overwrite/--no-overwrite', default=False)
@click.argument('input_path')
def save_tree(input_path, output_file, overwrite):
    """Saves a ascii tree visualisation of the chart of accounts"""
    loader = YamlLoader(input_path)
    tree = SaveAsTree(loader, output_file, do_overwrite=overwrite)
    tree.run()


if __name__ == '__main__':
    gtools()
