from pathlib import Path

import click
import piecash

from loaders import Loader
from model import AccountType


class SaveGnuCash:
    loader = None
    output_path = ""
    do_overwrite = False

    def __init__(self, loader: Loader, output_path="", do_overwrite=False):
        self.loader = loader
        self.output_path = self.__get_output_path(output_path)
        self.do_overwrite = do_overwrite

    def __get_output_path(self, output_path):
        path = Path(self.loader.input_path)

        if output_path == "":
            return path.rename(path.with_suffix('.gnucash')).path
        return output_path

    def run(self):
        data = self.loader.get_data()
        with piecash.create_book(self.output_path, currency="CHF", overwrite=self.do_overwrite) as book:
            self.__root_tree_walk(data, book)
            book.save()

    def __root_tree_walk(self, data, book):
        commodity = book.commodities.get(mnemonic="CHF")
        for child in data:
            account = piecash.Account(
                name="{} {}".format(child.number, child.name),
                type=AccountType(child.account_type).name,
                parent=book.root_account,
                commodity=commodity,
                placeholder=True,
                code=child.number
            )
            if child.children is not None:
                self.__sub_tree_walk(child, account, commodity)

    def __sub_tree_walk(self, item, parent, commodity):
        for child in item.children:
            is_placeholder = self.__is_placeholder(child)
            account = piecash.Account(
                name="{} {}".format(child.number, child.name),
                type=AccountType(child.account_type).name,
                parent=parent,
                commodity=commodity,
                placeholder=is_placeholder,
                code=child.number
            )
            if child.children is not None:
                self.__sub_tree_walk(child, account, commodity)

    @staticmethod
    def __is_placeholder(item):
        if item.children is None:
            return False

        if len(item.children) > 0:
            return True

        return False
