# GnuCash Tools

A collection of tools for [GnuCash](https://www.gnucash.org/). By [Genoessenschaft Solutionsbüro](https://buero.io) and the Theatregroup [Stgtk](https://gitlab.com/stgtk/).


## FromYaml

Takes a chart of accounts as a YAML file and converts it into an GnuCash (SqlLite) File.

### Input File

Example for such a file:
```yaml
- name: Aktiven
  number: 1
  type: asset
  children:
    - name: Umlaufvermögen
      number: 10
      type: asset
      children:
        - name: Flüssige Mittel
          number: 100
          type: asset
          children:
            - name: Bargeld
              number: 1000
              type: cash
              children: 
            - name: Raiffeisenbank Bern
              number: 1020
              type: bank
              children: 
[...]
- name: Passiven
  number: 2
  type: liability
  children:
    - name: Kurzfristiges Fremdkapital
      number: 20
      type: liability
      children:
        - name: Kurzfristige Ferbindlichkeiten
          number: 200
          type: liability
          children:
            - name: Verbindlichkeiten aus Lieferungen und Leistungen
              number: 2000
              type: payable
              children:
[...]
- name: Ertrag
  number: 3
  type: income
  placeholder: true
  children:
    - name: Produktionserlös
      number: 3000
      type: income
      placeholder: false
      children:
    - name: Handelserlös
      number: 3200
      type: income
      placeholder: false
      children:
[...]
```


### IntelliJ IDEA Live Templates

To speed up the authoring of such chart of accounts, it's possible to create a [Live Template](https://www.jetbrains.com/help/idea/using-live-templates.html) in [IntelliJ IDEA](https://www.jetbrains.com/idea).

The template:

```
- name: $name$
  number: $number$
  type: $type$
  children:$END$
```

For `$type$` set the following expression:

```
enum("asset","bank","cash","currency","equity","expense","income","receivable","liability","payable","stock")
```