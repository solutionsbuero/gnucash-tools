import abc
from pathlib import Path

import click
import yaml

from model import AccountSchema


class Loader:
    input_path = ""

    def __init__(self, input_path):
        if not Path(input_path).exists():
            click.echo("Input file doesn't exist")
            return
        self.input_path = input_path

    @abc.abstractmethod
    def get_data(self):
        pass


class YamlLoader(Loader):
    def __init__(self, input_path):
        super().__init__(input_path)

    def get_data(self):
        with open(self.input_path, 'r') as raw:
            try:
                raw_data = {"accounts": yaml.safe_load(raw)}
                data = AccountSchema().load(raw_data["accounts"], many=True).data
            except yaml.YAMLError as error:
                click.echo(error)

        return data
