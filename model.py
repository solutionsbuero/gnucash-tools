from enum import Enum
from marshmallow import Schema, fields, post_load


class AccountType(Enum):
    ASSET = "asset"
    BANK = "bank"
    CASH = "cash"
    CREDIT = "credit"
    EQUITY = "equity"
    EXPENSE = "expense"
    INCOME = "income"
    LIABILITY = "liability"
    MUTUAL = "mutual"
    PAYABLE = "payable"
    RECEIVABLE = "receivable"
    ROOT = "root"
    STOCK = "stock"
    TRADING = "trading"


class Account:
    def __init__(self, name, number, account_type, children):
        self.name = name
        self.number = number
        self.account_type = account_type
        self.children = children


class AccountSchema(Schema):
    class Meta:
        ordered = True

    name = fields.Str()
    number = fields.Int()
    account_type = fields.Str(load_from='type')
    children = fields.Nested('self', many=True, allow_none=True)

    @post_load
    def make_object(self, data):
        return Account(**data)
