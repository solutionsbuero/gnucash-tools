class SaveAsTree:
    loader = None
    output_path = ""
    do_overwrite = False
    indent_size = 2

    def __init__(self, loader, output_path, do_overwrite=False):
        self.output_path = output_path
        self.do_overwrite = do_overwrite
        self.loader = loader

    def run(self):
        accounts = self.loader.get_data()

        with open(self.output_path, 'w') as output_file:
            output_file.write(self.render(accounts))

    def render(self, accounts):
        result = ""

        for account in accounts:
            result = "{}├{} {} {}\n".format(result, "─" * self.indent_size, account.number, account.name)
            if account.children is not None:
                result = self.sub_render(account, result, 1, [0])

        return result

    def sub_render(self, enclosing_account, result, stage, lines_at):
        stage += 1

        # result = "{}│{}│\n".format(result, " " * (self.indent_size * stage - 1))

        i = 1
        for account in enclosing_account.children:
            if i == len(enclosing_account.children):
                branch_char = "└"
            else:
                branch_char = "├"

            line = "{}{}{}".format(self.render_indent(stage, lines_at), branch_char, "─" * self.indent_size)
            result = "{}{} {} {}\n".format(result, line, account.number, account.name)
            if account.children is not None:
                if branch_char == "├":
                    lines_at.append(stage * self.indent_size)
                elif len(account.children) > i:
                    lines_at = lines_at[-1:]
                result = self.sub_render(account, result, stage + 1, lines_at)
            i += 1

        return result

    def render_indent(self, stage, lines_at):
        result = ""
        length = stage * self.indent_size

        for i in range(length):
            if i in lines_at:
                result = "{}│".format(result)
            else:
                result = "{} ".format(result)

        return result
