import click
import yaml

from savegnucash import SaveGnuCash


class FromYaml(SaveGnuCash):
    input_path = ""
    output_path = ""
    do_overwrite = False

    # INIT

    def __init__(self, input_path, output_path="", do_overwrite=False):
        super().__init__(input_path, output_path, do_overwrite)
